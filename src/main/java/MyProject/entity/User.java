package MyProject.entity;

public class User {
    private String fullName;
    private String gender;
    private String position;
    private String password;
    private String email;
    private String urlPhoto;

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(String fullName, String gender, String position, String password, String email, String urlPhoto) {
        this.fullName = fullName;
        this.gender = gender;
        this.position = position;
        this.password = password;
        this.email = email;
        this.urlPhoto = urlPhoto;
    }

    @Override
    public String toString() {
        return "User{" +
                "fullName='" + fullName + '\'' +
                ", gender='" + gender + '\'' +
                ", position='" + position + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", urlPhoto='" + urlPhoto + '\'' +
                '}';
    }
}
