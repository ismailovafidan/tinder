package MyProject.service;

import MyProject.db.SqlConnection;
import MyProject.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegisterService {
    public void register(User user) throws SQLException {
        String db_Insert = "Insert into tinderuser(user_name,email,gender," +
                "password,profile_photo,position) values (?,?,?,?,?,?)";
        Connection connection = SqlConnection.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(db_Insert);
        preparedStatement.setString(1, user.getFullName());
        preparedStatement.setString(2, user.getEmail());
        preparedStatement.setString(3, user.getGender());
        preparedStatement.setString(4, user.getPassword());
        preparedStatement.setString(5, user.getUrlPhoto());
        preparedStatement.setString(6, user.getPosition());
        preparedStatement.execute();
        System.out.println("Insert process successfully ");
    }

    }
