package MyProject.servlets;

import MyProject.entity.User;
import MyProject.service.RegisterService;
import MyProject.template.TemplateEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterServlet extends HttpServlet {

    private final TemplateEngine engine;
    private RegisterService registerService;

    public RegisterServlet(TemplateEngine engine) {
        this.engine = engine;
    }
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        engine.render("register.ftl", resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        try {
            registerService.register(new User(
                    req.getParameter("username"),
                    req.getParameter("email"),
                    req.getParameter("gender"),
                    req.getParameter("password"),
                    req.getParameter("urlphoto"),
                    req.getParameter("position")
            ));
            resp.sendRedirect("/login/");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
